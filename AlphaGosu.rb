#!/usr/local/bin/ruby

require 'rubygems'
require 'gosu'

class TypedText < Gosu::TextInput
  attr_reader :x, :y
  def initialize(window, font, x, y)
    super()
    @window, @font, @x, @y = window, font, x, y
    self.text = ""
  end
  
  def draw
    # Finally, draw the text itself!
    @font.draw(self.text, x, y, 1,1)
  end
  
  def width
    @font.text_width(self.text)
  end
  
  def height
    @font.height
  end

  
end
    
class Letter
	attr_reader :image, :char
	
	def initialize(window)
	  @window = window
		letters = []
		('a'..'z').each do |letter|
			letters << letter
		end
		@letter = letters[rand(0-25)]
		@char = @letter.upcase
		#@image = "images/#{@letter}.png"
		@the_letter_image = Gosu::Image.new(@window, "images/#{@letter}.png" , true)
		if RUBY_PLATFORM.include? 'darwin'
		  Thread.new{@say_it = `arch -i386 osascript -e 'say "#{@letter}"'`}
	  end
	end
	
	def draw(x,y,z)
	  @the_letter_image.draw(x,y,z)
  end
end

class GameWindow < Gosu::Window
	def initialize
		super(480, 480, false)
		font = Gosu::Font.new(self, Gosu::default_font_name, 100)
		self.caption = "AlphaGosu"
		@my_letter = Letter.new(self)
		@background = Gosu::Image.new(self, 'images/bkgd.png', true)
		@happy = Gosu::Image.new(self, 'images/happy.png', true)
		@type_here = TypedText.new(self, font, 250, 450)
		@display_letter = Gosu::Font.new(self, Gosu::default_font_name, 100)
		self.text_input = @type_here
	  @width = self.width
	end

	def update
	  if @type_here.text.upcase == @my_letter.char
	    #puts "Hooray"
	    @my_letter = nil
	    @my_letter = Letter.new(self)
	    @my_letter.draw(15,15,15)
	    @type_here.text = nil
	    #@happy.draw(50,350,1)
	    #puts Gosu::milliseconds / 100 % @happy.width
	    
    else
      @type_here.text = nil
    end
	end	

	def draw
		@background.draw(0,0,0)
		@my_letter.draw(45,15,15)
		@type_here.draw
		@display_letter.draw(@my_letter.char, 360, 75, 1)
		# if @success == true
		#puts Gosu::milliseconds / 100 
	end

	
end

my_window = GameWindow.new
my_window.show
