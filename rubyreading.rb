Shoes.app(:title => 'Ruby Reading', :width => 500, :height => 500) do

<<<<<<< mine
  background "#fff"
  @notyping = false
  @platform = RUBY_PLATFORM
  def show_letter
    letters = []
    ('a'..'z').each do |letter|
      letters << letter
    end
    @letter = letters[rand(0-25)]
    @my_letter.replace(@letter, :align => "center", :size => 100 )
    @my_image = "images/#{@letter.downcase}.jpg"
    append do
  	  @pic = image "#{@my_image}", :left => 100, :top => 175, :width => 300, :height => 300
    end
  end

  def say_letter
    if @platform.include? 'darwin'
      Thread.new{@say_it = `osascript -e 'say "#{@my_letter}"'`}
    end
  end

  stack do
    flow do
      @my_letter = para ''
      show_letter
  	  say_letter
    end
    
    flow do
      @feedback = para "", :stroke => "red", :align => "center", :size => 50
    end


    ### gets keypress event and tests against @letter ###
   
      keypress do |k|
      unless @notyping
        if k == @letter.downcase
          @notyping = true
    	    @pic.remove
          @my_letter.replace('')
          append do
            @happy = image 'happy.gif', :left => 100, :top => 100, :width => 300
          end
          @star = star 50, 50, 5, 20, :fill => rgb(255,5,255)  
          @star_dance = animate(12) do |i|
            @star.move((0...400).rand, (0...400).rand)
            if i > 20
              @star_dance.remove
              @star.remove
              @happy.remove
              show_letter
    		      say_letter
    		      @notyping = false
            end
          end 
        else
          @notyping = true
          append do
            @sad = image 'sad.png', :left => 100, :top => 150, :width => 300
          end
          @sad_shake = animate(12) do |x|
            @sad.move((100...122).rand, (150))
            if x > 10
              @sad_shake.remove
              @sad.remove
              @notyping = false
    		      say_letter
            end
          end
        end
      end
    end
  end
end 

